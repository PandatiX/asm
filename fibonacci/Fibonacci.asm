.386
.MODEL flat, stdcall
option casemap:none

include c:\masm32\include\windows.inc
include c:\masm32\include\user32.inc
include c:\masm32\include\kernel32.inc
include c:\masm32\include\msvcrt.inc

includelib c:\masm32\lib\kernel32.lib
includelib c:\masm32\lib\user32.lib
includelib c:\masm32\lib\msvcrt.lib

.DATA
; variables initialisees
Result   DB    "Result: %d", 10, 0

.CODE
; int fibonacci(int n)
;
; Takes a number (from the stack) and returns
; its fib(n) (in eax).
;
; This lacks of quality (but directly comes
; from the myst code):
;  - n can be negative
;  - if n=0, the return value should be 0, not 1
;  - doesn't check for n being too big (overflow)
fibonacci PROC
    ; save stack
    push ebp
    mov ebp, esp

    mov ebx, [ebp+8] ; n

    sub esp, 16 ; i, j, k and l
    mov eax, 3
    mov [ebp-4], eax ; i = 3
    mov eax, 1
    mov [ebp-8], eax ; j = 1
    mov [ebp-12], eax ; k = 1
    ; [ebp-16] <=> l

iterate:
    ; if (i <= n)
    mov eax, [ebp-4]
    cmp eax, ebx
    ja done

    ; l = j + k
    mov eax, [ebp-8]
    add eax, [ebp-12]
    mov [ebp-16], eax

    ; j = k
    mov eax, [ebp-12]
    mov [ebp-8], eax

    ; k = l
    mov eax, [ebp-16]
    mov [ebp-12], eax

    ; i++
    mov eax, [ebp-4]
    inc eax
    mov [ebp-4], eax

    jmp iterate

done:
    ; return k
    mov eax, [ebp-12]

    ; restore stack
    mov esp, ebp
    pop ebp

    ret
fibonacci ENDP



start:
    push 11
    call fibonacci

    push eax
    push offset Result
    call crt_printf

	invoke ExitProcess, 0
END start
