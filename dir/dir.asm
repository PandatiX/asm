.386
.model flat,stdcall
option casemap:none

; Prototypes 
WinMain proto :DWORD, :DWORD, :DWORD
Dir     proto :PTR BYTE

include c:\masm32\include\windows.inc
include c:\masm32\include\user32.inc
include c:\masm32\include\kernel32.inc

includelib c:\masm32\lib\user32.lib
includelib c:\masm32\lib\kernel32.lib

.DATA
	currentDir      db    "C:\Users", 0
	ClassName       db    "Crappy Dir", 0
	AppName         db    "Crappy Explorer", 0
	ButtonClassName db    "button", 0
	EditClassName   db    "edit", 0
	ButtonText      db    "Dive !", 0
	lPathMax        equ   1024
	path            db    lPathMax dup (?)
	endLine         db    13, 10, 0
	maskFile        db    "*.*", 0
	slash           db    "\", 0
	dot             db    ".", 0
	dotDot          db    "..", 0
	depthprint      db    "  | ", 0
	depth           dword 0

.DATA?
	hInstance    HINSTANCE ?
	CommandLine  LPSTR     ?
	hwndButton   HWND      ?
	hwndEdit     HWND      ?
	hwndPrint    HWND      ?
	buffer       db        1024 dup(?)
	buff_endline db        ?

.CONST
	ButtonID    equ 1
	EditID      equ 2
	IDM_GETTEXT equ 3
	IDM_EXIT    equ 4

.CODE
start:
	invoke GetModuleHandle, NULL
	mov hInstance, eax

	invoke GetCommandLine
	mov CommandLine, eax

	invoke WinMain, hInstance, NULL, CommandLine

	invoke ExitProcess, eax



	;------------------------------------------------------------;
	;                            GUI                             ;
	;------------------------------------------------------------;

	; WinMain starts the main window. Directly returns the windows
	; message value.
	;
	; int WinMain(HINSTANCE hInst)
	WinMain PROC hInst:HINSTANCE, hPrevInst:HINSTANCE, CmdLine:LPSTR
		LOCAL wc:WNDCLASSEX
		LOCAL msg:MSG
		LOCAL hwnd:HWND
		mov   wc.cbSize,SIZEOF WNDCLASSEX
		mov   wc.style, CS_HREDRAW or CS_VREDRAW
		mov   wc.lpfnWndProc, OFFSET WndProc
		mov   wc.cbClsExtra, NULL
		mov   wc.cbWndExtra, NULL
		push  hInst
		pop   wc.hInstance
		mov   wc.hbrBackground, COLOR_BTNFACE + 1
		mov   wc.lpszMenuName, NULL
		mov   wc.lpszClassName, OFFSET ClassName

		invoke LoadIcon, NULL, IDI_APPLICATION

		mov wc.hIcon, eax
		mov wc.hIconSm, eax

		invoke LoadCursor, NULL, IDC_ARROW

		mov wc.hCursor, eax

		invoke RegisterClassEx, ADDR wc

		; Create the main windows
		invoke CreateWindowEx, WS_EX_CLIENTEDGE, ADDR ClassName, ADDR AppName, WS_SYSMENU, \
				CW_USEDEFAULT, CW_USEDEFAULT, \
				800, \ ; Y length
				900, \ ; X length 
				NULL, NULL, hInst, NULL
		mov hwnd, eax ; Store Windows UID 

		; Show Window
		invoke ShowWindow, hwnd, SW_SHOWNORMAL

		; Event handler (waits for the child win. to send states changes)
	eventhandle:
		invoke GetMessage, ADDR msg, NULL, 0, 0

		; .BREAK .IF (!eax)
		cmp eax, NULL
		jz done

		invoke TranslateMessage, ADDR msg
		invoke DispatchMessage, ADDR msg

		jmp eventhandle

    done:
		mov eax, msg.wParam

		ret
	WinMain ENDP

	; WndProc manages to build all the GUI.
	;
	; void WProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
	WndProc PROC hWnd:HWND, uMsg:UINT, wParam:WPARAM, lParam:LPARAM
		;.IF uMsg==WM_DESTROY
		cmp uMsg, WM_DESTROY
		jnz doNotquit

		invoke PostQuitMessage, NULL

		doNotquit:
		;.ELSEIF uMsg==WM_CREATE
		cmp uMsg, WM_CREATE
		jnz doNotCreate

		; Create user input windows
		invoke CreateWindowEx, WS_EX_STATICEDGE, ADDR EditClassName, ADDR currentDir, \
				ES_AUTOHSCROLL or ES_LEFT or WS_BORDER or WS_VISIBLE or WS_CHILD, \
				20, \
				35, \
				600, \
				25, \
				hWnd, EditID, hInstance, NULL

		mov hwndEdit, eax
		invoke SetFocus, hwndEdit

		; Create dive button		
		invoke CreateWindowEx, NULL, ADDR ButtonClassName, ADDR ButtonText, \
				WS_CHILD or WS_VISIBLE or BS_DEFPUSHBUTTON, \
				655, \
				35, \
				100, \
				25, \
				hWnd, ButtonID, hInstance, NULL

		mov  hwndButton, eax ; Save Windows UID

		; Create results window
		invoke CreateWindowEx, WS_EX_CLIENTEDGE, ADDR EditClassName, NULL, \
					WS_CHILD or WS_VISIBLE or WS_BORDER or ES_LEFT or\
					ES_AUTOHSCROLL or WS_HSCROLL or WS_VSCROLL or \
					ES_MULTILINE or ES_READONLY, \
				20, \
				100, \
				735, \
				695, \
				hWnd, EditID, hInstance, NULL

		mov hwndPrint, eax ; Save Windows UID

	doNotCreate:
		;.ELSEIF uMsg==WM_COMMAND
		cmp uMsg, WM_COMMAND
		jnz done

		mov eax, wParam
		;.IF lParam==0
		cmp lParam, 0
		jnz lParamNotZero

		;.IF ax==IDM_GETTEXT
		cmp ax, IDM_GETTEXT
		jnz notIDM_GETTEXT

		; Get path for input field to dive into
		invoke GetWindowText, hwndEdit, ADDR buffer, 512

		; Check if path contains a trailing '\'
		invoke lstrlen, ADDR buffer
		cmp byte ptr buffer[eax-1], '\'
		jz slashed
		
		; Add trailing '\'
		invoke lstrcat, ADDR buffer, ADDR slash

	slashed:
		; Start the Dir travel
		invoke lstrcpy, ADDR path, ADDR buffer
		invoke lstrlen, ADDR path
				
		; Mov effective addr of path
		lea eax, [path + eax]

		; Call core function
		invoke Dir, eax
				
		jmp done

	notIDM_GETTEXT:
		invoke DestroyWindow, hWnd
	
	lParamNotZero:
		;.IF ax==ButtonID
		cmp ax, ButtonID
		jnz done
		shr eax, 16

	;.IF ax==BN_CLICKED
	cmp ax, BN_CLICKED
	jnz done
	invoke SendMessage, hWnd, WM_COMMAND, IDM_GETTEXT, 0

	done:
		invoke DefWindowProc, hWnd, uMsg, wParam, lParam

		ret
	WndProc ENDP

	; PrintText writes a dword content (from the stack) into
	; the GUI.
	;
	; void PrintText(char* buff)
	PrintText PROC buff:DWORD
		LOCAL 	wparam:WPARAM
		LOCAL 	lparam:LPARAM

		invoke GetWindowTextLength, hwndPrint

		mov wparam, eax
		mov lparam, eax

		invoke SetFocus, hwndPrint
		invoke SendMessage, hwndPrint, EM_SETSEL, lparam, wparam
		invoke SendMessage, hwndPrint, EM_REPLACESEL, NULL, buff

		ret
	PrintText ENDP



	;------------------------------------------------------------;
	;                            CORE                            ;
	;------------------------------------------------------------;

	; PrintDepth prints the depth (from the global variable "depth") using
	; the separator string.
	;
	; void PrintDepth()
	PrintDepth PROC
		push ebp
		mov ebp, esp
		mov ebx, depth
	whiledepth:
		cmp ebx, 0
		je endwhiledepth
		push offset depthprint
		call PrintText
		dec ebx
		jmp whiledepth
	endwhiledepth:
		mov esp, ebp
		pop ebp
		ret
	PrintDepth ENDP

	; Dir travels recursively through the filesystem,
	; from a given directory path (from the stack).
	;
	; void Dir(char* myPath)
	Dir PROC myPath:PTR BYTE
		LOCAL filedata : WIN32_FIND_DATA

		push esi ; used for myPath
		push edi ; used for hFile HANDLE

		mov esi, myPath ; push myPath into the stack 
		invoke lstrcpy, esi, ADDR maskFile

		; find the first file in directory
		invoke FindFirstFile, ADDR path, ADDR filedata

		mov edi, eax ; save hFile

		; handle bad file handle
		cmp eax, INVALID_HANDLE_VALUE
		jz done

	whilenextfile:
		; check if the directory is not .
		lea eax, filedata.cFileName
		push eax
		push offset dot
		call lstrcmp
		cmp eax, NULL
		jz nextfile

		; check if the directory is not ..
		lea eax, filedata.cFileName
		push eax
		push offset dotDot
		call lstrcmp
		cmp eax, NULL
		jz nextfile

		; print depth
		call PrintDepth

		; print file name
		lea eax, filedata.cFileName
		push eax
		call PrintText
		push offset endLine
		call PrintText

		; check if is a directory
		mov eax, filedata.dwFileAttributes
		and eax, FILE_ATTRIBUTE_DIRECTORY
		cmp eax, FILE_ATTRIBUTE_DIRECTORY
		jnz nextfile ; is not a dir

		; concatenation of myPath and the new dir 
		invoke lstrcpy, esi, ADDR filedata.cFileName
		invoke lstrcat, esi, ADDR slash
		invoke lstrlen, esi
		lea eax, [esi + eax]

		; call Dir on the sub-dir
		inc depth
		invoke Dir, eax
		dec depth

	nextfile:
		invoke FindNextFile, edi, ADDR filedata

		or eax, eax
		jnz whilenextfile

		invoke FindClose, edi

	done:
		; free register and leave the current loop
		pop edi
		pop esi

		ret
	Dir ENDP
end start
