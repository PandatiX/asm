.386
.MODEL flat, stdcall
option casemap:none

include c:\masm32\include\windows.inc
include c:\masm32\include\user32.inc
include c:\masm32\include\kernel32.inc
include c:\masm32\include\msvcrt.inc

includelib c:\masm32\lib\kernel32.lib
includelib c:\masm32\lib\user32.lib
includelib c:\masm32\lib\msvcrt.lib

.DATA
; variables initialisees
Format   DB "%d", 0
Result DB "Result: %d", 10, 0

.DATA?
entered DD ?

.CODE
; int factorial(int n)
;
; Takes a number (from the stack) and returns
; n! (in eax).
; Notice that if n < 0, returns 1.
;
; This lacks of quality because it doesn't check
; for bounds (to avoid overflow), but we do not
; expect this there.
factorial PROC
    ; save stack
    push ebp
    mov ebp, esp

    mov ebx, [ebp+8] ; n

    sub esp, 8 ; i, r
    mov eax, 1
    mov [ebp-4], eax ; i = 1
    mov [ebp-8], eax ; r = 1

    ; if (n <= 0) return 1
    cmp ebx, 0
    jle one

    ; return factorial(n - 1) * n
    dec ebx
    push ebx
    call factorial ; factorial(n - 1)

    mov ebx, [ebp+8]
    mul ebx ; ... * n

    jmp done

one:
    mov eax, 1
    jmp done

done:
    ; restore stack
    mov esp, ebp
    pop ebp

    ret
factorial ENDP



start:
    ; ask for a number
    push offset entered
    push offset Format
    call crt_scanf

    ; compute its factorial
    push entered
    call factorial

    ; print result
    push eax
    push offset Result
    call crt_printf

	invoke ExitProcess, 0
END start
