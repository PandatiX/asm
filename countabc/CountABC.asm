.386
.MODEL flat, stdcall
option casemap:none

include c:\masm32\include\windows.inc
include c:\masm32\include\user32.inc
include c:\masm32\include\kernel32.inc
include c:\masm32\include\msvcrt.inc

includelib c:\masm32\lib\kernel32.lib
includelib c:\masm32\lib\user32.lib
includelib c:\masm32\lib\msvcrt.lib

.DATA
; variables initialisees
Phrase DB "aAbbBBcccCCC", 10, 0
Result DB "Counts: a:%d b:%d c:%d", 10, 0

.CODE
; void countabc(char* str)
;
; Takes a string (from the stack) and returns its
; number of 'a', 'b' and 'c' (respectively in eax,
; ebx and ecx).
countabc PROC
    ; save stack
    push ebp
    mov ebp, esp

    ; put string address in esi
    mov esi, dword ptr[ebp+8]

    ; create counta, countb and countc variables
    sub esp, 12
    mov eax, 0
    mov [ebp-4], eax ; counta = 0
    mov [ebp-8], eax ; countb = 0
    mov [ebp-12], eax ; countc = 0

job:
    mov al, [esi]

    ; if string end then done
    cmp al, 0
    je done

    ; if 'a'
    cmp al, 'a'
    je counta
    ; if 'b'
    cmp al, 'b'
    je countb
    ; if 'c'
    cmp al, 'c'
    je countc

    ; if another char then next
    jmp next

counta:
    mov eax, [ebp-4]
    inc eax
    mov [ebp-4], eax
    jmp next

countb:
    mov eax, [ebp-8]
    inc eax
    mov [ebp-8], eax
    jmp next

countc:
    mov eax, [ebp-12]
    inc eax
    mov [ebp-12], eax

next:
    mov [esi], al
    inc esi ; move to next char
    jmp job

done:
    ; return values in eax, ebx and ecx
    mov eax, [ebp-4]
    mov ebx, [ebp-8]
    mov ecx, [ebp-12]

    ; restore stack
    mov esp, ebp
    pop ebp

    ret
countabc ENDP



start:
    push offset Phrase
    call countabc

    push ecx
    push ebx
    push eax
    push offset Result
    call crt_printf

	invoke ExitProcess, 0
END start
