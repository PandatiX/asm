.386
.MODEL flat, stdcall
option casemap:none

include c:\masm32\include\windows.inc
include c:\masm32\include\user32.inc
include c:\masm32\include\kernel32.inc
include c:\masm32\include\msvcrt.inc

includelib c:\masm32\lib\kernel32.lib
includelib c:\masm32\lib\user32.lib
includelib c:\masm32\lib\msvcrt.lib

.DATA
; variables initialisees
Phrase       DB "Hello World !123", 10, 0
PhraseLength DB "Length: %d", 10, 0

.CODE
; void toupper(char* str)
;
; takes a string (from the stack) and converts its
; lowercase char to uppercase (remains on the stack).
toupper PROC
    ; save stack
    push ebp
    mov ebp, esp

    ; put string address in esi
    mov esi, dword ptr[ebp+8]

job:
    mov al, [esi]
    
    ; if string end then done
    cmp al, 0
    je done

    ; if less than 'a'
    cmp al, 'a'
    jl noChange
    cmp al, 'z'
    jle uppercase

    ; if not < 'a' and no <= 'z' then next
    jmp noChange

uppercase:
    sub al, 32 ; 'a' - 'A' = 32

noChange:
    mov [esi], al
    inc esi ; move to next char
    jmp job

done:
    ; restore stack
    mov esp, ebp
    pop ebp

    ret
toupper ENDP

; int strlen(char* str)
;
; takes a string (from the stack) and returns
; its length (eax).
strlen PROC
    ; save stack
    push ebp
    mov ebp, esp

    ; put string address in esi
    mov esi, dword ptr[ebp+8]

    ; len = 0
    sub esp, 4
    mov eax, 0
    mov [ebp-4], eax

next:
    mov al, [esi]
    
    ; if string end then done
    cmp al, 0
    je done

    ; len++
    mov eax, [ebp-4]
    inc eax
    mov [ebp-4], eax

    ; move to next char
    mov [esi], al
    inc esi
    jmp next

done:
    ; return len
    mov eax, [ebp-4]

    ; restore stack
    mov esp, ebp
    pop ebp

    ret
strlen ENDP



start:
    push offset Phrase

    call crt_printf
    call toupper
    call crt_printf

    call strlen
    push eax
    push offset PhraseLength
    call crt_printf
	
	invoke ExitProcess, 0
END start
