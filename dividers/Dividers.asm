.386
.MODEL flat, stdcall
option casemap:none

include c:\masm32\include\windows.inc
include c:\masm32\include\user32.inc
include c:\masm32\include\kernel32.inc
include c:\masm32\include\msvcrt.inc

includelib c:\masm32\lib\kernel32.lib
includelib c:\masm32\lib\user32.lib
includelib c:\masm32\lib\msvcrt.lib

.DATA
; variables initialisées
Format  DB "%d", 0
Decimal DB "%d ", 10, 0

.DATA?
entered DD ?

.CODE
; void dividers(int n)
;
; Takes a number n (from the stack), and prints
; all its dividers in stdout.
dividers PROC
    ; save stack
    push ebp
    mov ebp, esp

    mov ebx, [ebp+8] ; n

    ; i = 2
    sub esp, 4
    mov eax, 2
    mov [ebp-4], eax

    ; print 1 as a divider, as it's always true
    mov eax, 1
    push eax
    push offset Decimal
    call crt_printf

iterate:
    ; if (i <= n)
    mov eax, [ebp-4]
    cmp eax, ebx
    ja done

    ; compute modulus (edx = eax % ecx)
    xor edx, edx; reset edx TODO if it's really usefull. If true, why ?
    mov eax, ebx
    mov ecx, [ebp-4]
    div ecx
    
    ; if modulus then print it
    cmp edx, 0
    jne next
    
    mov eax, [ebp-4]
    push eax
    push offset Decimal
    call crt_printf    

next:
    ; i++
    mov eax, [ebp-4]
    inc eax
    mov [ebp-4], eax

    jmp iterate

done:
    ; restore stack
    mov esp, ebp
    pop ebp

    ret
dividers ENDP



start:
    ; ask for a number
    push offset entered
    push offset Format
    call crt_scanf

    ; compute and print its dividers
    push entered
    call dividers

	invoke ExitProcess, 0
END start
