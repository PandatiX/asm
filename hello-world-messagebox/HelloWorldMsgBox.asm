.386
.MODEL flat, stdcall
option casemap:none

include c:\masm32\include\windows.inc
include c:\masm32\include\user32.inc
include c:\masm32\include\kernel32.inc

includelib c:\masm32\lib\kernel32.lib
includelib c:\masm32\lib\user32.lib

.DATA
; variables initialisees
strTitle   DB "Hello World", 0
strMessage DB "42",          0

.CODE
start:
	invoke MessageBox, 0, ADDR strMessage, ADDR strTitle, MB_OK
	
	invoke ExitProcess, 0
END start
